package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Env struct {
	TestHostGRPC string `envconfig:"TEST_HOST_GRPC" default:"localhost:9012"`
}

func GetTestEnv() (Env, error) {
	var cfg Env
	err := envconfig.Process("", &cfg)
	return cfg, err
}
