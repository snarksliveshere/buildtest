// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package logsv1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// LogsServiceClient is the client API for LogsService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type LogsServiceClient interface {
	GetLogs(ctx context.Context, in *GetLogsRequest, opts ...grpc.CallOption) (*GetLogsResponse, error)
}

type logsServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewLogsServiceClient(cc grpc.ClientConnInterface) LogsServiceClient {
	return &logsServiceClient{cc}
}

func (c *logsServiceClient) GetLogs(ctx context.Context, in *GetLogsRequest, opts ...grpc.CallOption) (*GetLogsResponse, error) {
	out := new(GetLogsResponse)
	err := c.cc.Invoke(ctx, "/logs.v1.LogsService/GetLogs", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// LogsServiceServer is the server API for LogsService service.
// All implementations should embed UnimplementedLogsServiceServer
// for forward compatibility
type LogsServiceServer interface {
	GetLogs(context.Context, *GetLogsRequest) (*GetLogsResponse, error)
}

// UnimplementedLogsServiceServer should be embedded to have forward compatible implementations.
type UnimplementedLogsServiceServer struct {
}

func (UnimplementedLogsServiceServer) GetLogs(context.Context, *GetLogsRequest) (*GetLogsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetLogs not implemented")
}

// UnsafeLogsServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to LogsServiceServer will
// result in compilation errors.
type UnsafeLogsServiceServer interface {
	mustEmbedUnimplementedLogsServiceServer()
}

func RegisterLogsServiceServer(s grpc.ServiceRegistrar, srv LogsServiceServer) {
	s.RegisterService(&LogsService_ServiceDesc, srv)
}

func _LogsService_GetLogs_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetLogsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(LogsServiceServer).GetLogs(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/logs.v1.LogsService/GetLogs",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(LogsServiceServer).GetLogs(ctx, req.(*GetLogsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// LogsService_ServiceDesc is the grpc.ServiceDesc for LogsService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var LogsService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "logs.v1.LogsService",
	HandlerType: (*LogsServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetLogs",
			Handler:    _LogsService_GetLogs_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "logs/v1/logs.proto",
}
