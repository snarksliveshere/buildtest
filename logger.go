package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/snarksliveshere/buildtest/config"
	statsv1 "gitlab.com/snarksliveshere/buildtest/logger"

	"google.golang.org/grpc"
)

func main() {
	ctx := context.Background()
	cfg, err := config.GetTestEnv()
	if err != nil {
		log.Fatalf("error with get cfg, err: %v", err)
	}
	conn := getConnect(cfg.TestHostGRPC)
	client := statsv1.NewLogsServiceClient(conn)
	req := statsv1.GetLogsRequest{
		UserId:   "2180b051-fe49-11ea-affc-3822e2123bf9",
		Platform: "and",
		Limit:    100,
		Project:  "sma",
	}

	resp, err := client.GetLogs(ctx, &req)
	if err != nil {
		log.Fatalf("cant get stats from logger cause: %v", err)
	}
	fmt.Println(resp)
}

func getConnect(host string) *grpc.ClientConn {
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("error with connect to TestHostGRPC: %v", err)
	}
	fmt.Printf("Init api clients, connecting to %v...", host)
	return conn
}
